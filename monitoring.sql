/** List of all participants for each session (=project) **/

USE `librecs`;

DROP VIEW IF EXISTS v_learners;
CREATE VIEW v_learners AS
  SELECT
    EXTRACTVALUE(pr.config,'/projectConfig/@code') AS session,
    pa.participant_id,
    UPPER(us.name) AS name,
    LOWER(us.firstname) AS firstname,
    us.email
  FROM participants pa JOIN users us ON pa.user=us.account
    JOIN projects pr ON pr.project_id=pa.project_id
  WHERE pa.kind='L'
  AND EXTRACTVALUE(pr.config,'/projectConfig/@code') LIKE 'NA1_-18A'
  AND pa.participant_id NOT IN (
    'ey',
    'ff',
    'dh',
    'd3',
    'd4',
    'da',
    'fd',
    'db',
    'd8',
    'e1',
    'e2',
    'fg',
    'e9',
    'fa'
    )
  ORDER BY session, kind, name;

/** DETECT DOUBLON
select name, firstname, count(*) from v_learners group by name, firstname having count(*) > 1;
**/

/* Questionnaires */

DROP TABLE IF EXISTS questions;
CREATE TABLE questions (
  id VARCHAR(22) PRIMARY KEY,
  question VARCHAR(255),
  num TINYINT(1)
);
INSERT INTO questions VALUES ('FBtHlB1pmPd1GXhYrQVPre','Auto-évaluez votre degré de réussite au devoir',1);
INSERT INTO questions VALUES ('YRHZCzwAHOjzFTRfl2mddf','Auto-évaluez votre rapidité au devoir',2);
INSERT INTO questions VALUES ('oZOjlmTvYGbl4WydLvYXYe','Auto-évaluez votre capacité à résoudre ce type de devoir',3);
INSERT INTO questions VALUES ('V8Rpiqat0Ai8Ztgi4zvM0e','Combien de temps avez-vous passé',4);
INSERT INTO questions VALUES ('gp682sh37kj7LJXhYTPkkh','Évaluez la difficulté générale du contenu de ce module',5);
INSERT INTO questions VALUES ('PbumaXhnkMeRhZRQ6fmSpe','Évaluez l''intérêt général du contenu de ce module',6);


/** List of answers to questionnaires for each participant **/

DROP VIEW IF EXISTS v_answers;
CREATE VIEW v_answers AS
  SELECT
    l.session,
    RIGHT(LEFT(a.value,14),4) AS exercice,
    l.participant_id,
    l.name,
    l.firstname,
    q.id as question_id,
    q.num,
    q.question,
    JSON_UNQUOTE(JSON_EXTRACT(data,'$.r.field')) AS answer,
    f.data
  FROM records_assmntFields f JOIN anchors a ON f.anchor_id=a.anchor_id
    JOIN v_learners l ON l.participant_id=f.holder_id
    LEFT JOIN questions q ON RIGHT(a.value,22)=q.id
  WHERE question IS NOT NULL
  ORDER BY session, exercice, participant_id, q.num;

DROP VIEW IF EXISTS v_answers_anonymous;
CREATE VIEW v_answers_anonymous AS
  SELECT session, exercice, MD5(participant_id) AS anonymous, question_id, question, answer, data
  FROM v_answers;


/** Student monitoring **/

DROP VIEW IF EXISTS v_quizzes;
CREATE VIEW v_quizzes AS
  SELECT session, participant_id, name, firstname, exercice, ROUND(AVG(answer),1) AS autoeval
  FROM v_answers
  WHERE num<4
  GROUP BY session, exercice, participant_id, name, firstname
  ORDER BY session, name, firstname;

DROP VIEW IF EXISTS v_quizzes_anonymous;
CREATE VIEW v_quizzes_anonymous AS
  SELECT session, MD5(participant_id) AS anonymous, exercice, autoeval
  FROM v_quizzes;


/** CHECK Modules accessed at least once by user ?
Unknown event */

DROP VIEW IF EXISTS v_modules;
CREATE VIEW v_modules AS
  SELECT DISTINCT
    l.session,
    RIGHT(LEFT(a.value,14),4) AS exercice,
    l.participant_id,
    l.name,
    l.firstname
  FROM records_assmnt m JOIN anchors a ON m.anchor_id=a.anchor_id
    JOIN v_learners l ON l.participant_id=m.holder_id;


/** Access to module with or without answer to questionnaire **/

DROP VIEW IF EXISTS v_monitoring;
CREATE VIEW v_monitoring AS
  SELECT l.session, l.participant_id, l.name, l.firstname, m.exercice, q.autoeval
  FROM v_learners l
    LEFT JOIN v_modules m
      ON l.participant_id = m.participant_id
    LEFT OUTER JOIN v_quizzes q
      ON q.session=m.session
      AND q.exercice=m.exercice
      AND q.participant_id=m.participant_id
  /** Exclude first modules **/
  WHERE m.exercice NOT IN (
    'gen1',
    'mod1',
    'rel1',
    'rel2',
    'sql1',
    'alg1',
    'sql2'
    )
  ORDER BY session, name, firstname;
SELECT * FROM v_monitoring;

DROP VIEW IF EXISTS v_monitoring_anonymous;
CREATE VIEW v_monitoring_anonymous AS
  SELECT session, MD5(participant_id) AS anonymous, exercice, autoeval
  FROM v_monitoring;

DROP VIEW IF EXISTS v_monitoring_simple;
CREATE VIEW v_monitoring_simple AS
  SELECT session, participant_id, name, firstname, COUNT(exercice) AS accessed, COUNT(autoeval) AS filled, ROUND(AVG(autoeval),1) AS autoeval
  FROM v_monitoring
  GROUP BY session, participant_id, name, firstname;
SELECT * FROM v_monitoring_simple
WHERE name NOT IN
( 'TEST',
  'APPRENANT',
  'AMAROUCHE',
  'LUSSIER',
  'POINSART'
) ;

DROP VIEW IF EXISTS v_monitoring_simple_anonymous;
CREATE VIEW v_monitoring_simple_anonymous AS
  SELECT session, MD5(participant_id) AS anonymous, accessed, filled, autoeval
  FROM v_monitoring_simple;

/**
SELECT * FROM v_monitoring_simple_anonymous;
SELECT * FROM v_monitoring_anonymous where session="su-18p";
select session, exercice, anonymous, question, answer from v_answers_anonymous where session='su-18p' and anonymous = 'f3ea97d2cd1f5619f570c06a10a041b5' order by exercice;
**/
